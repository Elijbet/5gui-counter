import React, { useState } from 'react';
import './App.css';

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="Container">
      <input className="Left" value={count} readOnly />
      <button className="Right" onClick={() => setCount(count + 1)}>Count</button>
    </div>
  );
}

export default App;
